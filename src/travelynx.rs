use serde::Deserialize;
use serde_with::{
    serde_as,
    BoolFromInt
};

#[derive(Deserialize, Debug)]
pub struct Data {
    pub journeys: Vec<Journey>,
}

#[serde_as]
#[derive(Deserialize, Debug)]
pub struct Journey {
    pub arr_ds100: Option<String>,
    pub arr_eva: u64,
    pub arr_lat: f64,
    pub arr_lon: f64,
    pub arr_name: String,
    pub arr_platform: Option<String>,
    #[serde_as(as = "BoolFromInt")]
    pub cancelled: bool,
    pub checkin_ts: u64,
    pub checkout_ts: u64,
    pub dep_ts100: Option<String>,
    pub dep_eva: u64,
    pub dep_lat: f64,
    pub dep_lon: f64,
    pub dep_name: String,
    pub dep_platform: Option<String>,
    pub edited: i64,
    pub effective_visibility: i64,
    pub journey_id: i64,
    pub messages: Option<String>,
    pub polyline: Option<String>,
    pub real_arr_ts: u64,
    pub real_dep_ts: u64,
    pub route: String,
    pub sched_arr_ts: u64,
    pub sched_dep_ts: u64,
    pub train_id: String,
    pub train_line: Option<String>,
    pub train_no: String,
    pub train_type: String,
    pub user_data: Option<String>,
    pub user_id: u64,
    // pub visibility: Option<>,
}
