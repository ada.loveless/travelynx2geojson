use serde::Serialize;

use crate::travelynx;

#[derive(Serialize, Debug)]
pub struct Geometry {
    #[serde(rename(serialize = "type"))]
    type_: String,
    coordinates: Vec<Vec<f64>>,
}

#[derive(Serialize, Debug)]
pub struct Properties {
    pub arr_ds100: Option<String>,
    pub arr_eva: u64,
    pub arr_lat: f64,
    pub arr_lon: f64,
    pub arr_name: String,
    pub arr_platform: Option<String>,
    pub cancelled: bool,
    pub checkin_ts: u64,
    pub checkout_ts: u64,
    pub dep_ts100: Option<String>,
    pub dep_eva: u64,
    pub dep_lat: f64,
    pub dep_lon: f64,
    pub dep_name: String,
    pub dep_platform: Option<String>,
    pub edited: i64,
    pub effective_visibility: i64,
    pub journey_id: i64,
    pub messages: Option<String>,
    pub real_arr_ts: u64,
    pub real_dep_ts: u64,
    pub route: String,
    pub sched_arr_ts: u64,
    pub sched_dep_ts: u64,
    pub train_id: String,
    pub train_line: Option<String>,
    pub train_no: String,
    pub train_type: String,
    pub user_data: Option<String>,
    pub user_id: u64,
    // pub visibility: Option<>,
}

#[derive(Serialize, Debug)]
pub struct FeatureCollection {
    #[serde(rename(serialize = "type"))]
    pub type_: String,
    pub features: Vec<Feature>,
}

impl Into<FeatureCollection> for travelynx::Data {
    fn into(self) -> FeatureCollection {
        let features = self.journeys.iter().map(|journey| journey.into()).collect();
        FeatureCollection {
            type_: "FeatureCollection".into(),
            features,
        }
    }
}

#[derive(Serialize, Debug)]
pub struct Feature {
    #[serde(rename(serialize = "type"))]
    pub type_: String,
    pub geometry: Geometry,
    pub properties: Properties,
}

fn distance(x: &Vec<f64>, y: &Vec<f64>) -> f64 {
    x[0..2]
        .iter()
        .zip(&y[0..2])
        .map(|(x, y)| (x - y).powi(2))
        .sum::<f64>()
        .sqrt()
}

fn find_min(xs: Vec<f64>) -> Option<usize> {
    match xs.len() {
        0 => None,
        _ => {
            let mut i: usize = 0;
            let mut min: f64 = xs[0];
            for (j, x) in xs.iter().enumerate() {
                if *x < min {
                    min = *x;
                    i = j;
                }
            }
            Some(i)
        }
    }
}

/// Clip lines to the travelled journey
/// The lines otherwise contain the train's journey and not your journey.
/// This is approximate and may probably break in some cases.
/// Would be wiser to actually do some geometry but idk.
fn clip_line(coordinates: Vec<Vec<f64>>, start: &Vec<f64>, end: &Vec<f64>) -> Vec<Vec<f64>> {
    match coordinates.len() {
        0 => coordinates,
        _ => {
            let distances_start: Vec<f64> = coordinates
                .iter()
                .map(|coordinate| distance(coordinate, start))
                .collect();
            let index_start = find_min(distances_start).unwrap_or(0);
            let distances_end: Vec<f64> = coordinates
                .iter()
                .map(|coordinate| distance(coordinate, end))
                .collect();
            let index_end = find_min(distances_end).unwrap_or(coordinates.len());
            coordinates[index_start.min(index_end)..=index_end.max(index_start)]
                .iter()
                .map(|coordinate| coordinate[0..2].into())
                .collect()
        }
    }
}

impl Into<Feature> for &travelynx::Journey {
    fn into(self) -> Feature {
        let type_ = "Feature".into();
        let properties = Properties {
            arr_ds100: self.arr_ds100.clone(),
            arr_eva: self.arr_eva,
            arr_lat: self.arr_lat,
            arr_lon: self.arr_lon,
            arr_name: self.arr_name.clone(),
            arr_platform: self.arr_platform.clone(),
            cancelled: self.cancelled,
            checkin_ts: self.checkin_ts,
            checkout_ts: self.checkout_ts,
            dep_ts100: self.dep_ts100.clone(),
            dep_eva: self.dep_eva,
            dep_lat: self.dep_lat,
            dep_lon: self.dep_lon,
            dep_name: self.dep_name.clone(),
            dep_platform: self.dep_platform.clone(),
            edited: self.edited,
            effective_visibility: self.effective_visibility,
            journey_id: self.journey_id,
            messages: self.messages.clone(),
            real_arr_ts: self.real_arr_ts,
            real_dep_ts: self.real_dep_ts,
            route: self.route.clone(),
            sched_arr_ts: self.sched_arr_ts,
            sched_dep_ts: self.sched_dep_ts,
            train_id: self.train_id.clone(),
            train_line: self.train_line.clone(),
            train_no: self.train_no.clone(),
            train_type: self.train_type.clone(),
            user_data: self.user_data.clone(),
            user_id: self.user_id,
        };
        let coordinates: Vec<Vec<f64>> =
            serde_json::from_str(&self.polyline.clone().unwrap_or("".into())).unwrap_or(vec![
                vec![self.dep_lon, self.dep_lat],
                vec![self.arr_lon, self.arr_lat],
            ]);
        let geometry = Geometry {
            type_: "LineString".into(),
            coordinates: clip_line(
                coordinates,
                &vec![self.dep_lon, self.dep_lat],
                &vec![self.arr_lon, self.arr_lat],
            ),
        };
        Feature {
            type_,
            geometry,
            properties,
        }
    }
}
