mod geojson;
mod travelynx;

use std::io::Write;

use clap::Parser;

use format_serde_error::SerdeError;
use travelynx::Data;

use anyhow::Result;

/// Convert a travelynx data dump to geojson
#[derive(Parser, Debug)]
struct App {
    /// File to read
    // #[arg(short, long)]
    in_file: String,
    /// Where to output geojson. If blank will write to stdout
    #[arg(short, long)]
    out_file: Option<String>,
}

impl App {
    fn run(&self) -> Result<()> {
        let data = self.read()?;
        let result: geojson::FeatureCollection = data.into();
        let result_str = serde_json::to_string(&result)?;
        match &self.out_file {
            Some(handle) => {
                let mut file = std::fs::File::create(handle)?;
                write!(file, "{}\n", result_str)?;
            }
            None => println!("{}\n", result_str),
        };
        Ok(())
    }

    fn read(&self) -> Result<Data> {
        let content = std::fs::read_to_string(&self.in_file)?;
        let data: Data =
            serde_json::from_str(&content).map_err(|err| SerdeError::new(content, err))?;
        Ok(data)
    }
}

fn main() {
    let app = App::parse();
    match app.run() {
        Ok(_) => {}
        Err(error) => {
            eprintln!("{}", error);
        }
    };
}
