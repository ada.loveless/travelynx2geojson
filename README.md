# Convert travelynx data dump to GeoJSON

Converts a travelynx data dump to a GeoJSON.
The data dump refers to the dump provided to you in [your profile](https://travelynx.de/account)

## Usage

```
Usage: travelynx2geojson [OPTIONS] <IN_FILE>

Arguments:
  <IN_FILE>  File to read

Options:
  -o, --out-file <OUT_FILE>  Where to output geojson. If blank will write to stdout
  -h, --help                 Print help
```


## Problems

- Includes parts of the train run where the train got cancelled because it broke. Don't know how to deal with those.

